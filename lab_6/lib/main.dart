import 'package:flutter/material.dart';

import './accordion.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Hide the debug banner
      debugShowCheckedModeBanner: false,
      title: 'Frequently Asked Question',
      theme: ThemeData(
        backgroundColor: Colors.red,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.red,
          title: Text(
            'Frequently Asked Question',
          ),
        ),
        body: Column(children: [
          Accordion('Apakah Terapi Plasma Konvalesen itu?',
              'Terapi Plasma Konvalesen merupakan suatu cara pengobatan atau metode imunisasi pasif yang bertujuan sebagai terapi tambahan COVID-19 dengan memberikan plasma yang mengandung antibodi terhadap virus SARS-CoV-2 dari penyintas COVID-19 kepada pasien COVID-19 yang masih menderita penyakit tersebut atau sedang dirawat.'),
          Container(
            margin: EdgeInsets.all(1.0),
            child: FlatButton(
              height: 20,
              minWidth: 40,
              child: Text(
                'Delete',
                style: TextStyle(fontSize: 10.0),
              ),
              color: Colors.redAccent,
              textColor: Colors.white,
              onPressed: () {},
            ),
          ),
          Accordion(
              'Mengapa donor plasma hanya dapat dilakukan dalam kurun waktu 3 bulan setelah sembuh?',
              'Hal ini disebabkan karena umumnya kadar antibodi terhadap virus SARS-CoV-2 bertahan dalam kadar tinggi di dalam tubuh donor selama 3-4 bulan dan setelah itu akan menurun secara bertahap sehingga donor terbaik adalah yang baru sembuh dari COVID-19 selama kurun waktu tersebut.'),
          Container(
            margin: EdgeInsets.all(1.0),
            child: FlatButton(
              height: 20,
              minWidth: 40,
              child: Text(
                'Delete',
                style: TextStyle(fontSize: 10.0),
              ),
              color: Colors.redAccent,
              textColor: Colors.white,
              onPressed: () {},
            ),
          ),
          Accordion('Siapa saja yang bisa menerima TPK?',
              'Resipien TPK adalah penderita COVID-19 mulai dari stadium sedang, berat dan kritis. Walaupun demikian efektifitas TPK lebih optimal bila diberikan lebih dini (sejak awal), karena antibody di dalam plasma berfungsi menghilangkan virusnya bukan memperbaiki kerusakan organ yang sudah terjadi. Berdasarkan penelitian, TPK dapat mencegah perburukan penyakit dari sedang ke berat dan berat ke kritis.'),
          Container(
            margin: EdgeInsets.all(1.0),
            child: FlatButton(
              height: 20,
              minWidth: 40,
              child: Text(
                'Delete',
                style: TextStyle(fontSize: 10.0),
              ),
              color: Colors.redAccent,
              textColor: Colors.white,
              onPressed: () {},
            ),
          ),
        ]));
  }
}
