Nama  : Metta Permatasari
Kelas : PBP A
NPM   : 2006463761

//Apakah perbedaan antara JSON dan XML?//

JSON adalah singkatan dari JavaScript Object Notation.
JSON didesain menjadi self-describing, sehingga JSON sangat mudah untuk dimengerti.
JSON digunakan dibanyak aplikasi web maupun mobile, yaitu untuk menyimpan dan mengirimkan data.
XML adalah singkatan dari eXtensible Markup Language.
XML didesain menjadi self-descriptive, jadi dengan membaca XML tersebut kita bisa mengerti informasi apa yang ingin disampaikan dari data yang tertulis.
XML digunakan dibanyak aplikasi web maupun mobile, yaitu untuk menyimpan dan mengirimkan data.

Perbedaan JSON dan XML adalah XML hanyalah bahasa markup yang digunakan untuk menambahkan info tambahan ke teks biasa, sedangkan JSON adalah cara yang efisien untuk merepresentasikan data terstruktur dalam format yang dapat dibaca manusia.

JSON :
- Data disimpan seperti map dengan pasangan key-value
- Tidak menggunakan tags (menggunakan kurung kurawal, ada key dan ada value)
- Tidak dapat dituliskan comment
- Memiliki tipe data array, string, number, boolean
- Ukuran file lebih kecil
- Syntax lebih mudah dimengerti

XML :
- Data disimpan sebagai tree structure
- Menggunakan tags seperti HTML (menggunakan kurung sudut)
- Dapat dituliskan comment
- Memiliki tipe data string (yang nantinya dapat terkonversi menjadi gambar, chart, dan data-data lainnya)
- Ukuran file lebih besar
- Syntax lebih rumit


//Apakah perbedaan antara HTML dan XML?//

HTML (HyperText Markup Language) digunakan untuk membuat halaman web dan aplikasi web. Ini adalah bahasa komputer yang digunakan untuk menerapkan tata letak dan konvensi pemformatan ke dokumen teks. 

Perbedaan HTML dan XML :

XML berfokus pada transfer data sedangkan HTML difokuskan pada penyajian data.
XML didorong konten sedangkan HTML didorong oleh format.
XML itu Case Sensitive sedangkan XML Case Insensitive
XML menyediakan dukungan namespaces sementara HTML tidak menyediakan dukungan namespaces.
XML strict untuk tag penutup sedangkan HTML tidak strict.
Tag XML dapat dikembangkan sedangkan HTML memiliki tag terbatas.
Tag XML tidak ditentukan sebelumnya sedangkan HTML memiliki tag yang telah ditentukan sebelumnya.


